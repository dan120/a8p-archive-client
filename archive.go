package archive

type MetadataResponse struct {
	Result   int64           `json:"result"`
	Metadata ArchiveMetadata `json:"metadata"`
	Error    string          `json:"error"`
}

type ArchiveMetadata struct {
	Created        string            `json:"created"`
	IsFolder       bool              `json:"isfolder"`
	ParentFolderId int64             `json:"parentfolderid"`
	Icon           string            `json:"icon"`
	Id             string            `json:"id"`
	FileId         int64             `json:"fileid"`
	Path           string            `json:"path"`
	Modified       string            `json:"modified"`
	Thumb          bool              `json:"thumb"`
	FolderId       int64             `json:"folderid"`
	IsShared       bool              `json:"isshared"`
	IsMine         bool              `json:"ismine"`
	Name           string            `json:"name"`
	Size           int64             `json:"size"`
	Contents       []ArchiveMetadata `json:"contents"`
}

type FileOpenResponse struct {
	Result int64  `json:"result"`
	FD     int64  `json:"fd"`
	FileId int64  `json:"fileid"`
	Error  string `json:"error"`
}

type FileSizeResponse struct {
	Result int64 `json:"result"`
	Size   int64 `json:"size"`
	Offset int64 `json:"offset"`
}

type FileCloseResponse struct {
	Result int64 `json:"result"`
}

type UploadFileResponse struct {
	Result    int64             `json:"result"`
	Metadata  []ArchiveMetadata `json:"metadata"`
	FileIDs   []int64           `json:"fileids"`
	Checksums []FileChecksum    `json:"checksums"`
	Error     string            `json:"error"`
}

type FileChecksum struct {
	SHA1   string `json:"sha1"`
	SHA256 string `json:"sha256"`
}

type Archive interface {
	ListFolder(path string) (*MetadataResponse, error)
	CreateFolder(parentFolderId int64, name string) (folderId int64, err error)
	CreateFolderIfNotExists(parentFolderId int64, name string) (folderId int64, err error)
	UploadFileLocal(filePath string, targetFolderId int64) error
	DownloadFileLocal(fileId int64, localFile string) error
	CreateUploadLink(folderId int64) (uploadLinkId int64, uploadUri string, err error)
	FindFile(folderId int64, filename string) ([]ArchiveMetadata, error)
	GetZipFileDir(fileId int64, fileLength int64) ([]string, error)
	CreatePublicUrl(fileId int64) (linkId int64, link string, err error)
}
