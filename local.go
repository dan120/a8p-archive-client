package archive

import (
	"archive/zip"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log/slog"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type LocalArchiveConfig struct {
	RootFolder string
	Files      []LocalArchiveFile
}

type LocalArchiveFile struct {
	Id   int64
	Path string
}

type LocalArchive struct {
	Context    context.Context
	Config     LocalArchiveConfig
	NextFileId int64
	FileMap    map[int64]string
}

func NewLocalArchive(ctx context.Context, cfgFile string) (*LocalArchive, error) {
	// load configuration
	bytes, err := os.ReadFile(cfgFile)
	if err != nil {
		return nil, err
	}
	var c LocalArchiveConfig
	err = json.Unmarshal(bytes, &c)
	if err != nil {
		return nil, err
	}
	return NewLocalArchiveWithConfig(ctx, c)
}

func NewLocalArchiveWithConfig(ctx context.Context, c LocalArchiveConfig) (*LocalArchive, error) {

	if c.RootFolder == "" {
		return nil, fmt.Errorf("no root folder defined")
	}

	nextFileId := int64(1)

	// create file map
	fm := make(map[int64]string)
	fm[0] = c.RootFolder

	// add file map entries from config file
	for i := range c.Files {
		laf := c.Files[i]
		p := laf.Path
		if !strings.HasPrefix(p, c.RootFolder) {
			p = filepath.Join(c.RootFolder, p)
		}
		fm[laf.Id] = p
		if laf.Id >= nextFileId {
			nextFileId = laf.Id + 1
		}
	}

	a := &LocalArchive{
		Context:    ctx,
		Config:     c,
		FileMap:    fm,
		NextFileId: nextFileId,
	}

	slog.Debug("Local archive initialized", "nextFileId", nextFileId, "filemap", fm)

	return a, nil
}

func (p *LocalArchive) ListFolder(folderPath string) (*MetadataResponse, error) {
	s, err := url.QueryUnescape(folderPath)
	if err != nil {
		return nil, err
	}

	path := filepath.Join(p.Config.RootFolder, s)
	entries, err := os.ReadDir(path)
	if err != nil {
		return nil, err
	}

	fileId := p.getFileIdForPath(path)

	contents := make([]ArchiveMetadata, 0)
	for i := range entries {
		f := entries[i]
		newPath := filepath.Join(p.Config.RootFolder, f.Name())
		fid := p.getFileIdForPath(newPath)
		am := ArchiveMetadata{
			Id:   newPath,
			Name: f.Name(),
		}
		if f.IsDir() {
			am.IsFolder = true
			am.FolderId = fid
		} else {
			am.FileId = fid
		}
		contents = append(contents, am)
	}

	return &MetadataResponse{
		Result: 0,
		Metadata: ArchiveMetadata{
			Id:       path,
			FolderId: fileId,
			IsFolder: true,
			Contents: contents,
		},
	}, nil
}

func (p *LocalArchive) CreateFolder(parentFolderId int64, name string) (int64, error) {
	parentFolder := p.getPathForFileId(parentFolderId)
	if parentFolder == "" {
		return 0, fmt.Errorf("parent folder %d for %s not found", parentFolderId, name)
	}

	newFolderPath := filepath.Join(parentFolder, name)

	slog.Debug("Creating directory", "path", newFolderPath)

	err := os.Mkdir(newFolderPath, os.ModePerm)
	if err != nil {
		return 0, err
	}

	return p.getFileIdForPath(newFolderPath), nil
}

func (p *LocalArchive) CreateFolderIfNotExists(parentFolderId int64, name string) (int64, error) {
	parentFolder := p.getPathForFileId(parentFolderId)
	if parentFolder == "" {
		return 0, fmt.Errorf("parent folder %d for %s not found", parentFolderId, name)
	}

	newFolderPath := filepath.Join(parentFolder, name)

	slog.Debug("Creating directory", "path", newFolderPath)

	if _, err := os.Stat(newFolderPath); errors.Is(err, os.ErrNotExist) {
		err := os.Mkdir(newFolderPath, os.ModePerm)
		if err != nil {
			return 0, err
		}
		slog.Debug("Created folder", "name", newFolderPath)
	}

	return p.getFileIdForPath(newFolderPath), nil
}

func (p *LocalArchive) UploadFileLocal(filePath string, targetFolderId int64) error {
	slog.Debug("UploadFileLocal", "filePath", filePath, "targetFolderId", targetFolderId, "filemap", p.FileMap)

	dstSuffix := p.getPathForFileId(targetFolderId)
	if dstSuffix == "" {
		return fmt.Errorf("target folder ID not found: %d", targetFolderId)
	}
	dstPath := filepath.Join(dstSuffix, filepath.Base(filePath))

	p.getFileIdForPath(dstPath)

	return p.copyFile(dstPath, filePath)
}

func (p *LocalArchive) DownloadFileLocal(fileId int64, localFile string) error {
	slog.Debug("DownloadFileLocal", "fileId", fileId, "localFile", localFile)

	srcPath := p.getPathForFileId(fileId)
	if srcPath == "" {
		return fmt.Errorf("source file ID not found: %d", fileId)
	}

	if _, err := os.Stat(srcPath); errors.Is(err, os.ErrNotExist) {
		return fmt.Errorf("source file not found: %s", srcPath)
	}

	return p.copyFile(localFile, srcPath)
}

func (p *LocalArchive) CreateUploadLink(folderId int64) (uploadLinkId int64, uploadUri string, err error) {
	srcPath := p.getPathForFileId(folderId)
	if srcPath == "" {
		return 0, "", fmt.Errorf("folder ID not found: %d", folderId)
	}
	return 0, fmt.Sprintf("file://%s", srcPath), nil
}

func (p *LocalArchive) FindFile(folderId int64, filename string) ([]ArchiveMetadata, error) {
	srcPath := p.getPathForFileId(folderId)
	if srcPath == "" {
		return nil, fmt.Errorf("folder ID not found: %d", folderId)
	}

	slog.Debug("FindFile is searching", "folderId", folderId, "path", srcPath, "filename", filename)

	results := make([]ArchiveMetadata, 0)
	err := filepath.WalkDir(srcPath, func(path string, d fs.DirEntry, err error) error {
		if !d.IsDir() && d.Name() == filename {
			info, err := d.Info()
			if err != nil {
				return err
			}
			fileId := p.getFileIdForPath(path)
			results = append(results, ArchiveMetadata{
				FileId:  fileId,
				Name:    d.Name(),
				Path:    path,
				Created: info.ModTime().Format(time.RFC1123),
			})

		}
		return nil
	})

	return results, err
}

func (p *LocalArchive) GetZipFileDir(fileId int64, fileLength int64) ([]string, error) {
	var results []string = make([]string, 0)
	path := p.getPathForFileId(fileId)
	if path == "" {
		return nil, fmt.Errorf("unable to find path for file ID %d", fileId)
	}

	zf, err := zip.OpenReader(path)
	if err != nil {
		return nil, fmt.Errorf("unable to find path for file ID %d", fileId)
	}
	defer func() {
		zf.Close()
	}()
	for _, file := range zf.File {
		results = append(results, file.Name)
	}

	return results, nil
}

func (p *LocalArchive) CreatePublicUrl(fileId int64) (linkId int64, link string, err error) {
	path := p.getPathForFileId(fileId)
	if path == "" {
		return 0, "", fmt.Errorf("unable to find path for file ID %d", fileId)
	}
	return 1, fmt.Sprintf("file://%s", path), nil
}

func (p *LocalArchive) getNextFileId() int64 {
	i := p.NextFileId
	p.NextFileId = p.NextFileId + 1
	return i
}

func (p *LocalArchive) getPathForFileId(fileId int64) string {
	return p.FileMap[fileId]
}

func (p *LocalArchive) copyFile(dstPath string, srcPath string) error {
	slog.Debug("Copying file", "src", srcPath, "dst", dstPath)

	src, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer src.Close()

	dst, err := os.Create(dstPath)
	if err != nil {
		return err
	}
	defer dst.Close()

	if _, err := io.Copy(dst, src); err != nil {
		return err
	}

	return nil
}

func (p *LocalArchive) getFileIdForPath(path string) int64 {
	if !strings.HasPrefix(path, p.Config.RootFolder) {
		path = filepath.Join(p.Config.RootFolder, path)
	}

	for i, s := range p.FileMap {
		if s == path {
			return i
		}
	}

	fid := p.getNextFileId()
	p.FileMap[fid] = path
	slog.Debug("Added new local path", "fileId", fid, "path", path)

	return fid
}
