package archive

import (
	"context"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCreateFolder(t *testing.T) {
	ctx := context.Background()

	rootFolder, err := os.MkdirTemp("", "")
	defer func() {
		os.RemoveAll(rootFolder)
	}()
	require.NoError(t, err)

	archive, err := NewLocalArchiveWithConfig(ctx, LocalArchiveConfig{RootFolder: rootFolder})
	require.NoError(t, err)

	testFolderId, err := archive.CreateFolder(0, "testFolder")
	require.NoError(t, err)
	assert.Equal(t, int64(1), testFolderId)

	fi, err := os.Stat(filepath.Join(rootFolder, "testFolder"))
	require.NoError(t, err)
	assert.True(t, fi.IsDir())
}

func TestCreateTwoFolders(t *testing.T) {
	ctx := context.Background()

	rootFolder, err := os.MkdirTemp("", "")
	defer func() {
		os.RemoveAll(rootFolder)
	}()
	require.NoError(t, err)

	archive, err := NewLocalArchiveWithConfig(ctx, LocalArchiveConfig{RootFolder: rootFolder})
	require.NoError(t, err)

	testFolderId1, err := archive.CreateFolder(0, "testFolder1")
	require.NoError(t, err)
	assert.Equal(t, int64(1), testFolderId1)

	testFolderId2, err := archive.CreateFolder(0, "testFolder2")
	require.NoError(t, err)
	assert.Equal(t, int64(2), testFolderId2)

	fi, err := os.Stat(filepath.Join(rootFolder, "testFolder1"))
	require.NoError(t, err)
	assert.True(t, fi.IsDir())

	fi, err = os.Stat(filepath.Join(rootFolder, "testFolder2"))
	require.NoError(t, err)
	assert.True(t, fi.IsDir())
}

func TestCreateFolderAlreadyExists(t *testing.T) {
	ctx := context.Background()

	rootFolder, err := os.MkdirTemp("", "")
	defer func() {
		os.RemoveAll(rootFolder)
	}()
	require.NoError(t, err)

	err = os.Mkdir(filepath.Join(rootFolder, "testFolder"), os.ModePerm)
	require.NoError(t, err)

	archive, err := NewLocalArchiveWithConfig(ctx, LocalArchiveConfig{RootFolder: rootFolder})
	require.NoError(t, err)

	testFolderId, err := archive.CreateFolder(0, "testFolder")
	require.Error(t, err)
	assert.Equal(t, testFolderId, int64(0))
}

func TestCreateFolderIfNotExistsAlreadyExists(t *testing.T) {
	ctx := context.Background()

	rootFolder, err := os.MkdirTemp("", "")
	defer func() {
		os.RemoveAll(rootFolder)
	}()
	require.NoError(t, err)

	err = os.Mkdir(filepath.Join(rootFolder, "testFolder"), os.ModePerm)
	require.NoError(t, err)

	archive, err := NewLocalArchiveWithConfig(ctx, LocalArchiveConfig{RootFolder: rootFolder})
	require.NoError(t, err)

	testFolderId, err := archive.CreateFolderIfNotExists(0, "testFolder")
	require.NoError(t, err)
	assert.True(t, testFolderId > 0)
}

func TestFindFile(t *testing.T) {
	ctx := context.Background()

	rootFolder, err := os.MkdirTemp("", "")
	defer func() {
		os.RemoveAll(rootFolder)
	}()
	require.NoError(t, err)

	archive, err := NewLocalArchiveWithConfig(ctx, LocalArchiveConfig{RootFolder: rootFolder})
	require.NoError(t, err)

	testFolderId, err := archive.CreateFolder(0, "testFolder")
	require.NoError(t, err)
	assert.Equal(t, int64(1), testFolderId)

	testFolderPath := archive.getPathForFileId(testFolderId)
	assert.Equal(t, filepath.Join(rootFolder, "testFolder"), testFolderPath)

	file, err := os.Create(filepath.Join(testFolderPath, "processed.zip"))
	require.NoError(t, err)
	file.Close()

	metadata, err := archive.FindFile(testFolderId, "processed.zip")
	require.NoError(t, err)
	assert.Equal(t, 1, len(metadata))
	assert.Equal(t, filepath.Join(testFolderPath, "processed.zip"), metadata[0].Path)
	assert.Equal(t, int64(2), metadata[0].FileId)

	assert.Equal(t, "processed.zip", metadata[0].Name)
	assert.True(t, metadata[0].Created != "")
	time, err := time.Parse(time.RFC1123, metadata[0].Created)
	require.NoError(t, err)
	assert.NotNil(t, time)
}

func TestFindNewestFile(t *testing.T) {
	ctx := context.Background()

	rootFolder, err := os.MkdirTemp("", "")
	defer func() {
		os.RemoveAll(rootFolder)
	}()
	require.NoError(t, err)

	archive, err := NewLocalArchiveWithConfig(ctx, LocalArchiveConfig{RootFolder: rootFolder})
	require.NoError(t, err)

	testFolderId, err := archive.CreateFolder(0, "testFolder")
	require.NoError(t, err)
	assert.Equal(t, int64(1), testFolderId)

	testFolderPath := archive.getPathForFileId(testFolderId)
	assert.Equal(t, filepath.Join(rootFolder, "testFolder"), testFolderPath)

	file, err := os.Create(filepath.Join(testFolderPath, "processed.zip"))
	require.NoError(t, err)
	file.Close()

	metadata, err := archive.FindFile(testFolderId, "processed.zip")
	require.NoError(t, err)
	require.NotNil(t, metadata)
	assert.Equal(t, 1, len(metadata))
	assert.Equal(t, int64(2), metadata[0].FileId)

	nf, err := FindNewestFile(metadata)
	require.NoError(t, err)
	require.NotNil(t, nf)
	assert.Equal(t, int64(2), nf.FileId)

	assert.Equal(t, int64(3), archive.NextFileId)
}

func TestListFolder(t *testing.T) {
	ctx := context.Background()

	rootFolder, err := os.MkdirTemp("", "")
	defer func() {
		os.RemoveAll(rootFolder)
	}()
	require.NoError(t, err)

	parentFolder := filepath.Join(rootFolder, "foodir")

	archive, err := NewLocalArchiveWithConfig(ctx, LocalArchiveConfig{
		RootFolder: rootFolder,
		Files: []LocalArchiveFile{
			{
				Id:   1,
				Path: "foodir",
			},
			{
				Id:   2,
				Path: filepath.Join(parentFolder, "temp"),
			},
		},
	})
	require.NoError(t, err)

	assert.Equal(t, parentFolder, archive.getPathForFileId(1))
	assert.Equal(t, filepath.Join(parentFolder, "temp"), archive.getPathForFileId(2))

	mr, err := archive.ListFolder("/foodir")
	require.Error(t, err)
	assert.Nil(t, mr)
	assert.Equal(t, "", archive.getPathForFileId(3))

	err = os.Mkdir(parentFolder, os.ModePerm)
	require.NoError(t, err)

	mr, err = archive.ListFolder("/foodir")
	require.NoError(t, err)
	assert.NotNil(t, mr)
	assert.Equal(t, int64(1), mr.Metadata.FolderId)
	assert.Equal(t, parentFolder, archive.getPathForFileId(1))
	assert.Equal(t, filepath.Join(parentFolder, "temp"), archive.getPathForFileId(2))
	assert.Equal(t, "", archive.getPathForFileId(3))
}
