package archive

import (
	"archive/zip"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
)

type PcloudArchive struct {
	Context   context.Context
	UrlPrefix string
	ApiKey    string
	Client    *http.Client
}

type CreateUploadLinkResponse struct {
	Result       int64  `json:"result"`
	UploadLinkId int64  `json:"uploadlinkid"`
	Link         string `json:"link"`
	Mail         string `json:"mail"`
	Code         string `json:"code"`
}

type PubLinkResponse struct {
	Result int64  `json:"result"`
	LinkId int64  `json:"linkid"`
	Link   string `json:"link"`
	Code   string `json:"code"`
}

func (p PcloudArchive) ListFolder(path string) (*MetadataResponse, error) {
	var response MetadataResponse

	slog.Debug("pcloud.ListFolder", "path", path)

	// perform HTTP call
	err := p.doHttpGet(fmt.Sprintf("%s/listfolder?path=%s&nofiles=1&noshares=1&auth=%s", p.UrlPrefix, path, p.ApiKey), true, &response)
	if err != nil {
		return nil, err
	}

	// return error response if result was not 0
	if response.Result != 0 {
		return nil, errors.New(response.Error)
	}

	return &response, nil
}

func (p PcloudArchive) CreateFolder(parentFolderId int64, name string) (int64, error) {
	var response MetadataResponse

	slog.Debug("pcloud.CreateFolder", "parentFolderId", parentFolderId, "name", name)

	ename := url.QueryEscape(name)

	// perform HTTP call
	err := p.doHttpGet(fmt.Sprintf("%s/createfolder?folderid=%d&name=%s&auth=%s", p.UrlPrefix, parentFolderId, ename, p.ApiKey), true, &response)
	if err != nil {
		return 0, err
	}

	// return error response if result was not 0
	if response.Result != 0 {
		return 0, errors.New(response.Error)
	}

	// return created folder ID
	slog.Debug("Created folder", "name", response.Metadata.Name, "folderId", response.Metadata.FolderId, "parentFolderId", response.Metadata.ParentFolderId)
	return response.Metadata.FolderId, nil
}

func (p PcloudArchive) CreateFolderIfNotExists(parentFolderId int64, name string) (int64, error) {
	var response MetadataResponse

	slog.Debug("pcloud.CreateFolderIfNotExists", "parentFolderId", parentFolderId, "name", name)

	ename := url.QueryEscape(name)

	// perform HTTP call
	err := p.doHttpGet(fmt.Sprintf("%s/createfolderifnotexists?folderid=%d&name=%s&auth=%s", p.UrlPrefix, parentFolderId, ename, p.ApiKey), true, &response)
	if err != nil {
		return 0, err
	}

	// return error response if result was not 0
	if response.Result != 0 {
		return 0, errors.New(response.Error)
	}

	// return created folder ID
	slog.Debug("Created folder", "name", response.Metadata.Name, "folderId", response.Metadata.FolderId, "parentFolderId", response.Metadata.ParentFolderId)
	return response.Metadata.FolderId, nil
}

func (p PcloudArchive) FileOpen(fileId int64) (*FileOpenResponse, error) {
	var response FileOpenResponse

	// make open file call
	url := fmt.Sprintf("%s/file_open?flags=0&fileid=%d&auth=%s", p.UrlPrefix, fileId, p.ApiKey)
	req, err := http.NewRequestWithContext(p.Context, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Connection", "keep-alive")
	resp, err := p.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// read and parse response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(body, &response); err != nil {
		return nil, err
	}

	// return error response if result was not 0
	if response.Result != 0 {
		return nil, errors.New(response.Error)
	}

	return &response, nil
}

func (p PcloudArchive) FileSize(fd int64) (int64, error) {
	// make file pread call
	url := fmt.Sprintf("%s/file_size?fd=%d&auth=%s", p.UrlPrefix, fd, p.ApiKey)
	req, err := http.NewRequestWithContext(p.Context, http.MethodGet, url, nil)
	if err != nil {
		return 0, err
	}
	req.Header.Add("Connection", "keep-alive")
	resp, err := p.Client.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	// read the full response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	var response FileSizeResponse
	if err := json.Unmarshal(body, &response); err != nil {
		return 0, err
	}

	if response.Offset != 0 {
		return 0, fmt.Errorf("got unexpected offset: %d", response.Offset)
	}

	if response.Result != 0 {
		return 0, fmt.Errorf("got error response: %d", response.Result)
	}

	return response.Size, nil
}

func (p PcloudArchive) FilePread(fd int64, count int64, offset int64) ([]byte, error) {
	// make file pread call
	url := fmt.Sprintf("%s/file_pread?fd=%d&count=%d&offset=%d&auth=%s", p.UrlPrefix, fd, count, offset, p.ApiKey)
	req, err := http.NewRequestWithContext(p.Context, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Connection", "keep-alive")
	resp, err := p.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// read the full response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// if we get a JSON response, it was an error
	contentType := resp.Header.Get("Content-Type")
	if !strings.HasPrefix(contentType, "application/octet-stream") {
		if strings.HasPrefix(contentType, "application/json") {
			var response MetadataResponse
			err := json.Unmarshal(body, &response)
			if err != nil {
				return nil, fmt.Errorf("unexpected failure unmarshaling error JSON: %v", err)
			} else {
				return nil, errors.New(response.Error)
			}
		}
		return nil, fmt.Errorf("received unexpected content type: %s", contentType)
	}

	return body, nil
}

func (p PcloudArchive) FileClose(fd int64) {
	url := fmt.Sprintf("%s/file_close?fd=%d&auth=%s", p.UrlPrefix, fd, p.ApiKey)
	req, err := http.NewRequestWithContext(p.Context, http.MethodGet, url, nil)
	if err != nil {
		slog.Error("error closing file", "error", err)
	}
	resp, err := p.Client.Do(req)
	if err != nil {
		slog.Error("error closing file", "error", err)
	}
	defer resp.Body.Close()

	// read the full response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		slog.Error("error closing file", "error", err)
	}
	var response FileCloseResponse
	if err := json.Unmarshal(body, &response); err != nil {
		slog.Error("error closing file", "error", err)
	}
	if response.Result != 0 {
		slog.Error("error closing file", "result", response.Result)
	}
}

func (p PcloudArchive) UploadFileLocal(filePath string, targetFolderId int64) error {
	var response UploadFileResponse

	filename := url.QueryEscape(path.Base(filePath))

	slog.Debug("pcloud.UploadFile", "filePath", filePath, "fileName", filename, "targetFolderId", targetFolderId)

	// open the local file for reading
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	url := fmt.Sprintf("%s/uploadfile?folderid=%d&nopartial=1&auth=%s", p.UrlPrefix, targetFolderId, p.ApiKey)

	// create the form multi-part
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(filePath))
	if err != nil {
		return err
	}

	// copy the file data to the part
	_, err = io.Copy(part, file)
	if err != nil {
		return err
	}

	// close the writer
	err = writer.Close()
	if err != nil {
		return err
	}

	// create the HTTP POST request
	request, err := http.NewRequest("POST", url, body)
	if err != nil {
		return err
	}
	request.Header.Add("Content-Type", writer.FormDataContentType())

	// perform the request
	resp, err := p.Client.Do(request)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// read the response
	jbody, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	// convert response to JSON
	if err := json.Unmarshal(jbody, &response); err != nil {
		return err
	}

	// check that one file was acknowledged
	if len(response.FileIDs) != 1 {
		return fmt.Errorf("received unexpected number of file IDs: %d", len(response.FileIDs))
	}

	// verify file checksum of the uploaded file
	sha1, err := CreateSHA1Hash(filePath)
	if err != nil {
		return err
	}
	if response.Checksums[0].SHA1 != sha1 {
		return fmt.Errorf("checksum of uploaded file does not match: local=%s vs. remote=%s", sha1, response.Checksums[0].SHA1)
	}

	// return error response if result was not 0
	if response.Result != 0 {
		return errors.New(response.Error)
	}

	return nil
}

func (p PcloudArchive) DownloadFileLocal(fileId int64, localFile string) error {
	// open the file for reading
	fileOpenResponse, err := p.FileOpen(fileId)
	if err != nil {
		return err
	}
	defer p.FileClose(fileOpenResponse.FD)

	// get file size
	size, err := p.FileSize(fileOpenResponse.FD)
	if err != nil {
		return err
	}
	slog.Debug("File size retrieved", "fileId", fileId, "size", size, "fid", fileOpenResponse.FD)

	// create local file for output
	fo, err := os.Create(localFile)
	if err != nil {
		return err
	}
	defer func() {
		if err := fo.Close(); err != nil {
			slog.Error("Error closing local file: %v", err)
		}
	}()

	// read pCloud file to local file
	b, err := p.FilePread(fileOpenResponse.FD, size, 0)
	if err != nil {
		return err
	}

	// write to local file
	wrote, err := fo.Write(b)
	if err != nil {
		return err
	}
	if int64(wrote) != size {
		return fmt.Errorf("unable to write entire file; only wrote %d/%d bytes", wrote, size)
	}

	return nil
}

func (p PcloudArchive) CreateUploadLink(folderId int64) (int64, string, error) {
	// make create upload link call
	resp, err := http.Get(fmt.Sprintf("%s/createuploadlink?folderid=%d&comment=a8preservation.com_upload_request&auth=%s", p.UrlPrefix, folderId, p.ApiKey))
	if err != nil {
		return 0, "", err
	}
	defer resp.Body.Close()

	// read and parse response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, "", err
	}
	var createUploadLinkResponse CreateUploadLinkResponse
	if err := json.Unmarshal(body, &createUploadLinkResponse); err != nil {
		return 0, "", err
	}

	// verify that result is ok
	if createUploadLinkResponse.Result != 0 {
		return 0, "", fmt.Errorf("failed to create folder with status %d", createUploadLinkResponse.Result)
	}

	return createUploadLinkResponse.UploadLinkId, createUploadLinkResponse.Link, nil
}

func (p PcloudArchive) FindFile(folderId int64, filename string) ([]ArchiveMetadata, error) {
	var response []ArchiveMetadata

	// make list folder call
	resp, err := http.Get(fmt.Sprintf("%s/listfolder?folderid=%d&recursive=1&auth=%s", p.UrlPrefix, folderId, p.ApiKey))
	if err != nil {
		return response, err
	}
	defer resp.Body.Close()

	// read and parse response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return response, err
	}
	var listFolderResponse MetadataResponse
	if err := json.Unmarshal(body, &listFolderResponse); err != nil {
		return response, err
	}

	return FindFileRecursively(filename, &listFolderResponse.Metadata.Contents), nil
}

func (p PcloudArchive) GetZipFileDir(fileId int64, fileLength int64) ([]string, error) {
	// open the file for reading
	fileOpenResponse, err := p.FileOpen(fileId)
	if err != nil {
		return nil, err
	}

	// read the last 100 bytes of the file to look for the ZIP EOCD
	b, err := p.FilePread(fileOpenResponse.FD, 100, fileLength-100)
	if err != nil {
		return nil, err
	}

	// parse the EOCD from the returned file bytes
	eocd, eocdSize, err := ParseZipEOCD(b)
	if err != nil {
		return nil, err
	}

	var dataSize = fileLength - int64(eocdSize) - int64(eocd.CDSize)
	offset := fileLength - dataSize

	// read the CD + EOCD
	buf, err := p.FilePread(fileOpenResponse.FD, dataSize, offset)
	if err != nil {
		return nil, err
	}

	// use the zip utils to read the zip file directory
	reader, err := zip.NewReader(bytes.NewReader(buf), dataSize)
	if err != nil {
		return nil, err
	}

	var files []string
	for _, file := range reader.File {
		// append a '/' to the name if it's a directory
		if file.FileInfo().IsDir() {
			files = append(files, fmt.Sprintf("%s/", file.Name))
		} else {
			files = append(files, file.Name)
		}
	}

	return files, nil
}

func (p PcloudArchive) CreatePublicUrl(fileId int64) (linkId int64, link string, err error) {
	// make create folder call
	resp, err := http.Get(fmt.Sprintf("%s/getfilepublink?fileid=%d&auth=%s", p.UrlPrefix, fileId, p.ApiKey))
	if err != nil {
		return 0, "", err
	}
	defer resp.Body.Close()

	// read and parse response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, "", err
	}
	var pubLinkResponse PubLinkResponse
	if err := json.Unmarshal(body, &pubLinkResponse); err != nil {
		return 0, "", err
	}

	// verify that result is ok
	if pubLinkResponse.Result != 0 {
		return 0, "", fmt.Errorf("failed to create public link with status %d", pubLinkResponse.Result)
	}

	return pubLinkResponse.LinkId, pubLinkResponse.Link, nil
}

func (p PcloudArchive) doHttpGet(url string, keepAlive bool, response interface{}) error {
	// make HTTP call
	req, err := http.NewRequestWithContext(p.Context, http.MethodGet, url, nil)
	if err != nil {
		return err
	}
	if keepAlive {
		req.Header.Add("Connection", "keep-alive")
	}
	resp, err := p.Client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	// read and parse response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(body, response); err != nil {
		return err
	}
	return nil
}
