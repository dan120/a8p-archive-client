package archive

import (
	"log/slog"
	"strings"
	"time"
)

func FindNewestFile(files []ArchiveMetadata) (*ArchiveMetadata, error) {
	var result *ArchiveMetadata

	if files != nil {
		var foundTime time.Time
		var foundFile ArchiveMetadata
		found := false
		for _, f := range files {
			if f.Created != "" {
				t, err := time.Parse(time.RFC1123, f.Created)
				if err != nil {
					return nil, err
				}
				if foundTime.Before(t) {
					foundFile = f
					foundTime = t
					found = true
					slog.Debug("Found newer file", "fileId", f.FileId, "name", f.Name, "date", f.Created)
				}
			}
		}
		if found {
			result = &foundFile
		}
	}

	return result, nil
}

func FindFileRecursively(filename string, contents *[]ArchiveMetadata) []ArchiveMetadata {
	var foundFiles []ArchiveMetadata

	filename = strings.ToLower(filename)

	if contents != nil {
		for _, f := range *contents {
			if f.IsFolder {
				ff := FindFileRecursively(filename, &f.Contents)
				if len(ff) > 0 {
					foundFiles = append(foundFiles, ff...)
				}
			} else if strings.ToLower(f.Name) == filename {
				foundFiles = append(foundFiles, f)
			}
		}
	}

	return foundFiles
}
