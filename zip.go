package archive

import (
	"bytes"
	"encoding/binary"
	"errors"
)

type EOCD struct {
	Signature     int32
	DiskNumber    int16
	DiskStart     int16
	CDCount       int16
	CDTotal       int16
	CDSize        int32
	CDOffset      int32
	CommentLength int16
}

// Parse the ZIP file end of central directory (must be taken from the last 22 bytes of the file)
func ParseZipEOCD(buffer []byte) (*EOCD, int, error) {
	// EOCD can be variable length due to comments, so look for start of EOCD signature
	var soeocd uint32 = 0x06054b50
	sb := make([]byte, 4)
	binary.LittleEndian.PutUint32(sb, soeocd)
	offset := bytes.Index(buffer, sb)
	if offset == -1 {
		return nil, 0, errors.New("ZIP EOCD not found")
	}

	// if we found the EOCD, read the structure
	eocd := EOCD{}
	err := binary.Read(bytes.NewBuffer(buffer[offset:]), binary.LittleEndian, &eocd)
	if err != nil {
		return nil, 0, err
	}

	return &eocd, len(buffer) - offset, nil
}
